<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>juegoTest</title>
    <link rel="stylesheet" href="\pruebaQUIZ\css\preguntas.css">
</head>
<body>
<?php
session_start();
$puntos2=0;
if(isset($_POST["pregunta2"])){
    $opcion=$_POST["pregunta2"];
    switch($opcion){
        case "A":
            $puntos2 = $puntos2+6;
            break;
        case "B":
            $puntos2 = $puntos2+3;
            break;
        case "C":
            $puntos2 = $puntos2+1;
            break;
    }
    $_SESSION["pregunta2"]= $puntos2;
}

?>

    <form action="\pruebaQUIZ\PREGUNTAS\pregunta4.php"  method="post">
        <div class="colocar">
                <div class="caja">
        <p>3.-¿Cuál de estos artistas es conocido como el "Rey del Pop" y es famoso por álbumes como "Thriller" y "Bad"?</p>
        <label class="ed">
            <input type="radio" name="pregunta3" value="A" required>
            Michael Jackson <br>
            <input type="radio" name="pregunta3" value="B" required>
            Prince <br>
            <input type="radio" name="pregunta3" value="C" required>
            Elvis Presley<br><br>
            <button type="submit" name="visitas">Siguiente</button> 
        </label>
    </div>  
        </div>
    </form>

</body>
</html>
