<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>juegoTest</title>
    <link rel="stylesheet" href="\pruebaQUIZ\css\preguntas.css">
</head>
<body>
<?php
    $puntos4 = 0;
    session_start();
    if(isset($_POST["pregunta4"])){
        $opcion=$_POST["pregunta4"];
        switch($opcion){
            case "A":
                $puntos4 = $puntos4+6;
                break;
            case "B":
                $puntos4 = $puntos4+3;
                break;
            case "C":
                $puntos4 = $puntos4+1;
                break;
        }
        $_SESSION["pregunta4"] = $puntos4;
    }
?>  
    <form action="\pruebaQUIZ\resultado.php"  method="post">
        
    <div class="colocar">
        <div class="caja">
        <p>5.-¿Cuál es tu preferencia de voz en un cantante?</p>
        <label class="ed">
            <input type="radio" name="pregunta5" value="A" required>
            Voz suave y melódica <br>
            <input type="radio" name="pregunta5" value="B" required>
            Voz potente y enérgica <br>
            <input type="radio" name="pregunta5" value="C" required>
            Voz rasgada y llena de emoción <br><br>
            <button type="submit" name="visitas"> Siguiente</button>
        </label>
    </div> 
    </div>
    </form>

</body>
</html>
