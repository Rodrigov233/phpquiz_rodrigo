<?php
// Datos de conexión a la base de datos (igual que en resultado.php)
$ip = "192.168.56.101:3306";
$database = "Quiz";
$user = "admin";
$pass = "admin";

// Establecer conexión con la base de datos
$conexion = mysqli_connect($ip, $user, $pass) or die ("No se ha podido conectar a la base de datos");

// Seleccionar la base de datos
mysqli_select_db($conexion, $database) or die ("No existe la base de datos");

// Consultar los datos de la tabla Jugadores
$consulta = "SELECT * FROM jugadores";

// Ejecutar la consulta
$resultado = mysqli_query($conexion, $consulta);
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tablero</title>
    <link rel="stylesheet" href="\pruebaQUIZ\css\tabla.css">
</head>
<body>
    <video autoplay muted loop id="videoFondo">
        <source src="/pruebaQUIZ/img/tabla.mp4" type="video/mp4">
    </video>
    <div class="conteiner">
    <h1>Tablero de Jugadores</h1>
    <table border ="2" class="tabla">
        <tr>
            <th>ID</th>
            <th>Nombre</th>
            <th>Puntuación</th>
            <th>Resultado</th>
            <th>Fecha</th>
        </tr>
        <?php while ($fila = mysqli_fetch_assoc($resultado)) { ?>
            <tr>
                <td><?php echo $fila['id']; ?></td>
                <td><?php echo $fila['nombre']; ?></td>
                <td><?php echo $fila['puntuacion']; ?></td>
                <td><?php echo $fila['resultado']; ?></td>
                <td><?php echo $fila['fecha']; ?></td>
            </tr>
        <?php } ?>
    </table>
    <a class='texto' href="index.php">Regresar al Inicio</a>
    </div>
</body>
</html>

<?php
// Cerrar la conexión
mysqli_close($conexion);
?>
