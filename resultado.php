<?php
session_start();
$puntos_totales = 0;
for ($i = 1; $i <= 4; $i++) {
    $pregunta_key = "pregunta".$i;
    if (isset($_SESSION[$pregunta_key])) {
        $puntos_totales += $_SESSION[$pregunta_key];
    }
}

// Obtener el nombre del usuario 
$nombre = isset($_COOKIE["nombre"]) ? $_COOKIE["nombre"] : "";

// Determina el artista según la puntuación
$artista = "";
if ($puntos_totales < 5) {
    $artista = "Enrique Iglesias";
    echo '<div class="circulo"><img src="\PRUEBAQUIZ\cantantes\iglesias.png"></div>';
    echo '<audio id="audioPlayer" controls class="audio-player">';
    echo '<source src="/PruebaQuiz/img/Iglesias.mp3" type="audio/mpeg">';
    echo '</audio>';
} elseif ($puntos_totales < 10) {
    $artista = "Michael jackson";
    echo '<div class="circulo"><img src="\PRUEBAQUIZ\cantantes\michaeljackson.png"></div>';
    echo '<audio id="audioPlayer" controls class="audio-player">';
    echo '<source src="/PruebaQuiz/img/Jackson5.mp3" type="audio/mpeg">';
    echo '</audio>';
} elseif ($puntos_totales < 15) {
    $artista = "Axel Roses";
    echo '<div class="circulo"><img src="\PRUEBAQUIZ\cantantes\axel.png"></div>';
    echo '<audio id="audioPlayer" controls class="audio-player">';
    echo '<source src="/PruebaQuiz/img/Paradise.mp3" type="audio/mpeg">';
    echo '</audio>';
} elseif ($puntos_totales < 20) {
    $artista = "Freddie Mercurie";
    echo '<div class="circulo"><img src="\PRUEBAQUIZ\cantantes\freddie.png"></div>';
    echo '<audio id="audioPlayer" controls class="audio-player">';
    echo '<source src="/PruebaQuiz/img/Queen.mp3" type="audio/mpeg">';
    echo '</audio>';
} elseif ($puntos_totales < 25) {
    $artista = "Manu Chao";
    echo '<div class="circulo"><img src="\PRUEBAQUIZ\cantantes\manu.png"></div>';
    echo '<audio id="audioPlayer" controls class="audio-player">';
    echo '<source src="/PruebaQuiz/img/Manuchao.mp3" type="audio/mpeg">';
    echo '</audio>';
}

// Datos de conexión a la base de datos
$ip = "192.168.56.101:3306";
$database = "Quiz";
$user = "admin";
$pass = "admin";

// Establecer conexión con la base de datos
$conexion = mysqli_connect($ip, $user, $pass) or die ("No se ha podido conectar a la base de datos");

// Seleccionar la base de datos
mysqli_select_db($conexion, $database) or die ("No existe la base de datos");

// Insertar los datos del usuario en la tabla de jugadores
$fecha = date("Y-m-d");
$insert_query = "INSERT INTO jugadores (nombre, puntuacion, resultado, fecha) VALUES ('$nombre', $puntos_totales, '$artista', '$fecha')";
mysqli_query($conexion, $insert_query);

// Cerrar la conexión
mysqli_close($conexion);
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Resultados</title>           
    <link rel="stylesheet" href="/PRUEBAQUIZ/css/resultado.css">
</head>
<body>
<div class="container">
    <?php
    echo "<div class='texto'>";
    echo "<p>Puntos totales: $puntos_totales puntos</p>";
    echo "<p>El artista musical que coincide con tus puntos es:<br> $artista </p>";
    echo "</div>";
    ?>
    <a class='texto' href="tabla.php">Tabla de jugadores</a>
</div>
<script>
    // Espera a que el documento HTML esté completamente cargado
    document.addEventListener("DOMContentLoaded", function() {
        // Para luego obtener el elemento de audio
        var audioPlayer = document.getElementById("audioPlayer");
        // y asi lograr reproducir el audio automáticamente
        audioPlayer.play().catch(function(error) {
            console.log("Error al reproducir el audio automáticamente:", error);
        });
        // Oculta los controles del reproductor de audio
        audioPlayer.controls = false;
    });
</script>
</body>
</html>
