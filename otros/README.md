**PREGUNTAS QUIZ**

**Pregunta1:** 
<form action="\pruebaQUIZ\PREGUNTAS\pregunta2.php"  method="post">
        <div class="colocar">
            <div class="caja">
                <p>1.-¿Cuál de los siguientes géneros musicales te hace sentir más energizado y listo para bailar?</p>
                <label class="ed">
                    <input type="radio" name="pregunta1" value="A" required>
                    Electrónica<br>
                    <input type="radio" name="pregunta1" value="B" required>
                    Rock <br>
                    <input type="radio" name="pregunta1" value="C" required>
                    Reggaetón<br><br>
                    <button type="submit" name="visitas" id="botonEnviar">Siguiente</button> 
                </label>
            </div>
        </div>
    </form>

**Pregunta2:** 
    <form action="\pruebaQUIZ\PREGUNTAS\pregunta3.php" method="post">
        <div class="colocar">
            <div class="caja">
                <p>2.-¿Qué tipo de letra de canción prefieres?</p>
                <label class="ed">
                    <input type="radio" name="pregunta2" value="A" required>
                    Letras románticas y melódicas <br>
                    <input type="radio" name="pregunta2" value="B" required>
                    Letras profundas y reflexivas<br>
                    <input type="radio" name="pregunta2" value="C" required>
                    Piano <br><br>
                    <button type="submit" name="visitas">Siguiente</button> 
                </label>
            </div>
        </div>
    </form>


```php

<?php
session_start();
$puntos1=0;
if(isset($_POST["pregunta1"])){
    $opcion=$_POST["pregunta1"];
    switch($opcion){
        case "A":
            $puntos1 = $puntos1+6;
            break;
        case "B":
            $puntos1 = $puntos1+3;
            break;
        case "C":
            $puntos1 = $puntos1+1;
            break;
    }
    $_SESSION["pregunta1"]= $puntos1;
}
?>
```


**Pregunta3:**  
<form action="\pruebaQUIZ\PREGUNTAS\pregunta4.php"  method="post">
        <div class="colocar">
                <div class="caja">
        <p>3.-¿Cuál de estos artistas es conocido como el "Rey del Pop" y es famoso por álbumes como "Thriller" y "Bad"?</p>
        <label class="ed">
            <input type="radio" name="pregunta3" value="A" required>
            Michael Jackson <br>
            <input type="radio" name="pregunta3" value="B" required>
            Prince <br>
            <input type="radio" name="pregunta3" value="C" required>
            Elvis Presley<br><br>
            <button type="submit" name="visitas">Siguiente</button> 
        </label>
    </div>  
        </div>
    </form>

```php

<?php
session_start();
$puntos2=0;
if(isset($_POST["pregunta2"])){
    $opcion=$_POST["pregunta2"];
    switch($opcion){
        case "A":
            $puntos2 = $puntos2+6;
            break;
        case "B":
            $puntos2 = $puntos2+3;
            break;
        case "C":
            $puntos2 = $puntos2+1;
            break;
    }
    $_SESSION["pregunta2"]= $puntos2;
}
?>
```
**Pregunta4:**  
 <form action="\pruebaQUIZ\PREGUNTAS\pregunta5.php"  method="post">
    <div class="colocar">
        <div class="caja">
        <p>4.-¿Qué instrumento musical te gusta más escuchar en una canción?</p>
        <label class="ed">
            <input type="radio" name="pregunta4" value="A" required>
            Guitarra eléctrica<br>
            <input type="radio" name="pregunta4" value="B" required>
            Batería <br>
            <input type="radio" name="pregunta4" value="C" required>
            Piano <br><br>
            <button type="submit" name="visitas">Siguiente</button> 
        </label> 
    </div>
    </div>
    </form>

```php

<?php
session_start();
$puntos3=0;
if(isset($_POST["pregunta3"])){
    $opcion=$_POST["pregunta3"];
    switch($opcion){
        case "A":
            $puntos3 = $puntos3+6;
            break;
        case "B":
            $puntos3 = $puntos3+3;
            break;
        case "C":
            $puntos3 = $puntos3+1;
            break;
    }
    $_SESSION["pregunta3"]= $puntos3;
}
?>
```

**Pregunta5:**  
    <form action="\pruebaQUIZ\resultado.php"  method="post"> 
    <div class="colocar">
        <div class="caja">
        <p>5.-¿Cuál es tu preferencia de voz en un cantante?</p>
        <label class="ed">
            <input type="radio" name="pregunta5" value="A" required>
            Voz suave y melódica <br>
            <input type="radio" name="pregunta5" value="B" required>
            Voz potente y enérgica <br>
            <input type="radio" name="pregunta5" value="C" required>
            Voz rasgada y llena de emoción <br><br>
            <button type="submit" name="visitas"> Siguiente</button>
        </label>
    </div> 
    </div>
    </form>

```php
<?php
    $puntos4 = 0;
    session_start();
    if(isset($_POST["pregunta4"])){
        $opcion=$_POST["pregunta4"];
        switch($opcion){
            case "A":
                $puntos4 = $puntos4+6;
                break;
            case "B":
                $puntos4 = $puntos4+3;
                break;
            case "C":
                $puntos4 = $puntos4+1;
                break;
        }
        $_SESSION["pregunta4"] = $puntos4;
    }
?>
```


**resultado.php:**
<body>  
<div class="container">
    <?php
    echo "<div class='texto'>";
    echo "<p>Puntos totales: $puntos_totales puntos</p>";
    echo "<p>El artista musical que coincide con tus puntos es:<br> $artista </p>";
    echo "</div>";
    ?>
    <a class='texto' href="tabla.php">Tabla de jugadores</a>
</div>
<script>
    // Espera a que el documento HTML esté completamente cargado
    document.addEventListener("DOMContentLoaded", function() {
        // Para luego obtener el elemento de audio
        var audioPlayer = document.getElementById("audioPlayer");
        // y asi lograr reproducir el audio automáticamente
        audioPlayer.play().catch(function(error) {
            console.log("Error al reproducir el audio automáticamente:", error);
        });
        // Oculta los controles del reproductor de audio
        audioPlayer.controls = false;
    });
</script>
</body>
```php
<?php
session_start();
$puntos_totales = 0;
for ($i = 1; $i <= 4; $i++) {
    $pregunta_key = "pregunta".$i;
    if (isset($_SESSION[$pregunta_key])) {
        $puntos_totales += $_SESSION[$pregunta_key];
    }
}
// Obtener el nombre del usuario 
$nombre = isset($_COOKIE["nombre"]) ? $_COOKIE["nombre"] : "";

// Determina el artista según la puntuación
$artista = "";
if ($puntos_totales < 5) {
    $artista = "Enrique Iglesias";
    echo '<div class="circulo"><img src="\PRUEBAQUIZ\cantantes\iglesias.png"></div>';
    echo '<audio id="audioPlayer" controls class="audio-player">';
    echo '<source src="/PruebaQuiz/img/Iglesias.mp3" type="audio/mpeg">';
    echo '</audio>';
} elseif ($puntos_totales < 10) {
    $artista = "Michael jackson";
    echo '<div class="circulo"><img src="\PRUEBAQUIZ\cantantes\michaeljackson.png"></div>';
    echo '<audio id="audioPlayer" controls class="audio-player">';
    echo '<source src="/PruebaQuiz/img/Jackson5.mp3" type="audio/mpeg">';
    echo '</audio>';
} elseif ($puntos_totales < 15) {
    $artista = "Axel Roses";
    echo '<div class="circulo"><img src="\PRUEBAQUIZ\cantantes\axel.png"></div>';
    echo '<audio id="audioPlayer" controls class="audio-player">';
    echo '<source src="/PruebaQuiz/img/Paradise.mp3" type="audio/mpeg">';
    echo '</audio>';
} elseif ($puntos_totales < 20) {
    $artista = "Freddie Mercurie";
    echo '<div class="circulo"><img src="\PRUEBAQUIZ\cantantes\freddie.png"></div>';
    echo '<audio id="audioPlayer" controls class="audio-player">';
    echo '<source src="/PruebaQuiz/img/Queen.mp3" type="audio/mpeg">';
    echo '</audio>';
} elseif ($puntos_totales < 25) {
    $artista = "Manu Chao";
    echo '<div class="circulo"><img src="\PRUEBAQUIZ\cantantes\manu.png"></div>';
    echo '<audio id="audioPlayer" controls class="audio-player">';
    echo '<source src="/PruebaQuiz/img/Manuchao.mp3" type="audio/mpeg">';
    echo '</audio>';
}
// Datos de conexión a la base de datos
$ip = "192.168.56.101:3306";
$database = "Quiz";
$user = "admin";
$pass = "admin";

// Establecer conexión con la base de datos
$conexion = mysqli_connect($ip, $user, $pass) or die ("No se ha podido conectar a la base de datos");

// Seleccionar la base de datos
mysqli_select_db($conexion, $database) or die ("No existe la base de datos");

// Insertar los datos del usuario en la tabla de jugadores
$fecha = date("Y-m-d");
$insert_query = "INSERT INTO jugadores (nombre, puntuacion, resultado, fecha) VALUES ('$nombre', $puntos_totales, '$artista', '$fecha')";
mysqli_query($conexion, $insert_query);

// Cerrar la conexión
mysqli_close($conexion);
?>
```
