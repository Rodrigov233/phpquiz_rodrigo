<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>JuegoTest</title>
    <link rel="stylesheet" href="/PruebaQUIZ/css/inicio.css">
</head>
<body>
    <div class="titulo">
        <h1>JUEGO TIPO TEST</h1>
    </div>  
    <video autoplay muted loop id="videoFondo">
        <source src="/pruebaQUIZ/img/fondo.mp4" type="video/mp4">
    </video>
    <section class="cajon">
        <div class="caja">
            <div class="caja__inicio">
                <h2>Descubre Tu Artista Musical</h2>
                <div><img src="./img/fotos-conciertos-.gif" alt=""></div> 
            </div>
            <form action="\pruebaQUIZ\bienvenida.php" method="POST">
                <label>
                    <br>
                    <input type="text" name="nombre"  placeholder="Ingrese nombre" required><br> 
                    <button type="submit" name="iniciar"><div class="boton">Jugar!</div></button>
                </label>
            </form>
        </div>
    </section>
</body>
</html>
